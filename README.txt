About
=====

This is a utility function, intended to help other modules and javascript
determine if a given IP is within certain ranges.

Usage
=====

Configure the module at admin/config/system/iptest.

Send IP's to the web service URL to determine if the IP is in one of the
configured ranges.

For example, if the module were set to:

  10.0.0.1 - 10.255.255.255

This request:

  iptest/rangetest/8.8.8.8

Should return:

  "0"

...which is the string "0" in JSON

This request:

  iptest/rangetest/10.1.1.1

Should return:

  "1"

...which is the string "1" in JSON.

An address URL parameter is not mandatory. If one is not sent, the client's
IP address is determined by ip_address().
